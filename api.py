from flask import Flask,request,jsonify,render_template
# import matplotlib.pyplot as plt
import nltk
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import roc_auc_score, roc_curve
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer

app = Flask(__name__)

@app.route("/",methods=['GET','POST'])
def index():
    return render_template("index.html")

@app.route("/analyze",methods=['GET','POST'])
def analyze():
    text = [request.form["review"]]

    column_name = ['Review', 'Target']
    df = pd.read_csv('data/imdb_reviews.csv', sep='\t', names=column_name)
    print(df)
    X_train, X_test, y_train, y_test = train_test_split(df['Review'], df['Target'], test_size=0.33)
    tft = TfidfVectorizer()
    tft.fit(X_train)
    trans = tft.transform(X_train)
    model = MultinomialNB(alpha=0.1)
    model.fit(trans, y_train)
    predictions = model.predict(tft.transform(text))
    if(predictions==0):
        comment = "It is a negative review"
    else:
        comment = "It is a positive review"

    return render_template('index.html',comment=comment)




if __name__=='__main__':
    app.run(debug=True ,port='5000')
